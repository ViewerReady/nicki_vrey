{
    "_versionMajor": 0,
    "_versionMinor": 3,
    "boundsCenter": {
        "x": 0,
        "y": 0,
        "z": 1.348679780960083
    },
    "boundsSize": {
        "x": 3.0457355976104736,
        "y": 1.7132261991500854,
        "z": 1.0116276741027832
    },
    "format": "perpixel",
    "numAngles": 1,
    "perspectives": [
        {
            "clipEpsilon": 0.004942529834806919,
            "crop": {
                "w": 0.961693525314331,
                "x": 0.26008063554763794,
                "y": 0.038306452333927155,
                "z": 0.4959677457809448
            },
            "depthFocalLength": {
                "x": 3740.97119140625,
                "y": 3740.97119140625
            },
            "depthImageSize": {
                "x": 6144.0,
                "y": 3456.0
            },
            "depthPrincipalPoint": {
                "x": 3057.69091796875,
                "y": 1690.1568603515625
            },
            "extrinsics": {
                "e00": 0.9996330738067627,
                "e01": 0.0014787106774747372,
                "e02": 0.027046315371990204,
                "e03": -0.0032740591559559107,
                "e10": -0.004647332709282637,
                "e11": 0.993065357208252,
                "e12": 0.11747150123119354,
                "e13": -0.07107032835483551,
                "e20": -0.02668505348265171,
                "e21": -0.11755409836769104,
                "e22": 0.9927079081535339,
                "e23": 0.04000278562307358,
                "e30": 0.0,
                "e31": 0.0,
                "e32": 0.0,
                "e33": 1.0
            },
            "farClip": 1.8544936180114746,
            "nearClip": 0.8428659439086914
        }
    ],
    "textureHeight": 6656,
    "textureWidth": 3056
}
